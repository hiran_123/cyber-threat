import React from 'react';
import { Container, Grid, Divider, AppBar, Toolbar, Box, TextField, Button, Typography, Hidden, Link, FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import logo from '../../assets/redd_logo_white.svg';
import logo2 from '../../assets/brightredd_logo_white.svg';
import logo3 from '../../assets/cyber_logo_white.svg';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import AddIcon from '@mui/icons-material/Add';
import '../global.scss'; 

function TabPanel(props) {
   const { children, value, index, ...other } = props;
 
   return (
     <div
       role="tabpanel"
       hidden={value !== index}
       id={`simple-tabpanel-${index}`}
       aria-labelledby={`simple-tab-${index}`}
       {...other}
     >
       {value === index && (
         <Box sx={{ p: 3 }}>
           <Typography>{children}</Typography>
         </Box>
       )}
     </div>
   );
 }

TabPanel.propTypes = {
   children: PropTypes.node,
   index: PropTypes.number.isRequired,
   value: PropTypes.number.isRequired,
 };
 

function a11yProps(index) {
   return {
     id: `simple-tab-${index}`,
     'aria-controls': `simple-tabpanel-${index}`,
   };
 }

 export default function BasicTabs() {
   const [value, setValue] = React.useState(0);
 
   const handleChange = (event, newValue) => {
     setValue(newValue);
   };

   
    return (
         
      <Container className="container-holder page-intro" maxWidth={false} disableGutters={true}>
         <AppBar position="static" elevation={0}>
            <Toolbar> 
               <img src={logo} className="logo" alt="" />
               <img src={logo3} className="logo logo3" alt="" />
            </Toolbar>    
         </AppBar>  
         <Grid 
                container
                direction="row"
                justify="flex-start"
                alignItems="stretch"
                item={true}
            >
                <Hidden only="xs">
                    <Grid item sm={3} className="left-panel"> 
                        <div className="inner">
                            <img src={logo2} className="logo2" alt="" />                             
                        </div>    
                                         
                    </Grid>
                </Hidden>
                <Grid item sm={9} className="right-panel">
                    <div className="inner-top">
                        <div className="inner">                        
                            <Typography variant="h1">Welcome to Cyber Threat Assessment</Typography>      
                            <Divider />                      
                            <Typography variant="subtitle1">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec commodo eros 
ut gravida interdum. Duis sed nibh tempus, vestibulum ex porttitor, ultricies tellus. Proin id ex massa. </Typography>  
                        </div>
                        <Hidden smUp>
                            <div className="left-panel">
                                <div className="inner"></div>
                            </div>                                
                        </Hidden>

                        <Box sx={{ borderBottom: 1, borderColor: 'divider' }} className="wizard-holder">
                           <Tabs value={value} onChange={handleChange} variant="scrollable" scrollButtons={false} aria-label="basic tabs example">
                              <Tab label="Email verification" {...a11yProps(0)} className="step-done"></Tab>
                              <Tab label="Select company" {...a11yProps(1)} />
                              <Tab label="Verify domain names" {...a11yProps(2)} />
                           </Tabs>
                        </Box>
                    </div>                    
                    
                    <div className="inner inner-bottom"> 
                        <TabPanel value={value} index={0}>

                           {/* default view */}
                           <div className="initial-step">
                              <Typography variant="subtitle1">Please enter your email address to start the process.</Typography>       
                              <form noValidate autoComplete="off" className="form-holder width-half">
                                 <TextField name="email" label="Email address" variant="outlined" />
                              </form>
                              <Button color="secondary" variant="contained">
                                 Submit
                              </Button>
                           </div>

                           {/* verify email step: by default it's hidden */}
                           <div className="verify-step" style={{ display: 'none' }}>
                              <Typography variant="subtitle1">A verification code has been emailed to email-address. Enter the code to verify your email address.</Typography>       
                              <form noValidate autoComplete="off">
                                 <Box display="grid" className="form-holder">
                                    <TextField name="email" label="6-Digit Code" variant="outlined" />
                                 </Box>
                              </form>
                              <Typography variant="subtitle2">Didn't receive code? <Link color="secondary" variant="subtitle2">Re-send again</Link></Typography>
                              <br />
                              <Button color="secondary" variant="contained">
                                 Verify email address
                              </Button>
                           </div>

                        </TabPanel>
                        
                        <TabPanel value={value} index={1}>
                           
                           <div className="initial-step">
                              <Typography variant="subtitle1">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s!</Typography>
                              <FormControl variant="outlined" className="form-holder width-half">
                                    <InputLabel id="demo-simple-select-outlined-label">Select a company</InputLabel>
                                    <Select
                                        defaultValue = ""
                                        labelId="demo-simple-select-outlined-label"
                                        id="demo-simple-select-outlined"
                                        label="Select a company"
                                    >               
                                        <MenuItem value="Yes - Managed Services Provider">PeachIT-Digital</MenuItem>
                                        <MenuItem value="Yes - local IT person">PeachIT-Digital 2</MenuItem>
                                        <MenuItem value="No - I have an Internal IT Manager">PeachIT-Digital 3</MenuItem>
                                    </Select>
                              </FormControl>
                              <br />
                              <Button color="secondary" variant="contained">
                                 Continue
                              </Button>
                           </div>

                        </TabPanel>

                        <TabPanel value={value} index={2}>
                           
                           <Typography variant="subtitle1">Please add valid email addresses for the domains that you wish to scan below.</Typography>   
                           <Typography variant="subtitle2">Maximum allowed 3 domains only!</Typography>     
                           <form noValidate autoComplete="off">
                              <Box display="grid" className="form-holder vertical">
                                 <ul className="state-done">
                                    <li><TextField label="example@example.com" variant="outlined" /></li>
                                    <li>
                                       <CheckCircleIcon color="primary" />
                                       <Button color="primary" variant="contained">
                                          Verify email
                                       </Button>
                                    </li>
                                 </ul>
                                 <ul>
                                    <li><TextField label="example@example.com" variant="outlined" /></li>
                                    <li>
                                       <Button color="primary" variant="contained" className="cta-responsive"> 
                                          Verify email
                                       </Button>
                                    </li>
                                 </ul>
                                 
                                 <Link className="link-add-domain">
                                    <AddIcon color="primary" />
                                    Add another domain
                                 </Link>
                              </Box>
                           </form>

                           <hr /><br />
                           <Button color="secondary" variant="contained">
                              Scan domains
                           </Button>
                        </TabPanel>  
                        

                        <Typography variant="subtitle2" id="copyright" property="dc:rights">
                            <span property="dc:publisher">&copy; REDD Digital Services Pty Ltd</span>        
                        </Typography>
                    </div>
                </Grid>                
            </Grid>
      </Container>        

    )
 }
 

 //export default step1;