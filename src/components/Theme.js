import { createMuiTheme } from '@material-ui/core';
import createBreakpoints from '@material-ui/core/styles/createBreakpoints'

const breakpoints = createBreakpoints({});

export const theme = createMuiTheme({
    MuiCssBaseline: {
      '@global': {
        html: {
          WebkitFontSmoothing: "auto"
        }
      },
    },
    palette: {
      primary: {
        main: '#595959',
      },
      secondary: {
        main: '#ED242E',
      },
      text: {
        primary: 'rgba(89, 89, 89, 1)',
        disabled: 'rgba(0, 0, 0, 0.38)',
      },
      background: {
        default: "#F0F2F5"
      }
    },    
    typography: {
      htmlFontSize: 16,
      fontFamily: "'TT Norms Pro', sans-serif",
      fontSize: 14,
      lineHeight: 18,
      fontWeightLight: 300,
      fontWeightRegular: 400,
      fontWeightMedium: 500,
      fontWeightBold: 700,
      h1: {
        fontFamily: "'TT Norms Pro', sans-serif",
        fontWeight: 700,
        fontSize: "2rem",
        lineHeight: "2.4rem",
        paddingBottom: "1rem",
        color: '#ED242E',
        [breakpoints.up('md')]: {
          fontSize: "2.6rem",
          lineHeight: "3.2rem",
        }
      },
      h2: {
        fontFamily: "'TT Norms Pro', sans-serif",
        fontWeight: 700,
        fontSize: "1.1rem",
        lineHeight: "1.4rem",
        paddingBottom: ".5rem",
        [breakpoints.up('md')]: {
          fontSize: "1.2rem",
          lineHeight: "1.6rem",
        }
      },
      h3: {
        fontFamily: "'TT Norms Pro', sans-serif",
        fontWeight: 700,
        fontSize: "1rem",
        lineHeight: "1.4rem",
      },
      h4: {
        fontFamily: "'TT Norms Pro', sans-serif",
        fontWeight: 700,
        fontSize: "1.4rem",
        lineHeight: "1.8rem",
        paddingBottom: ".5rem",
      },
      subtitle1: {
        fontFamily: "'TT Norms Pro', sans-serif",
        fontWeight: 400,
        fontSize: "1.1rem",
        lineHeight: "1.6rem"
      },
      subtitle2: {
        fontFamily: "'TT Norms Pro', sans-serif",
        fontWeight: 400,
        fontSize: ".9rem",
        lineHeight: "1.3rem",
      },
      button: {
        fontFamily: "'TT Norms Pro', sans-serif",
        fontSize: "1.1rem",
        lineHeight: "1.1rem",
      }
    },    
    overrides: {
      MuiButton: {
        root: {
          borderRadius: 0,
          textTransform: 'none',
          padding: '.8rem 1.5rem',
          [breakpoints.up('lg')]: {
            padding: '.8rem 2.5rem',
          }
        },
        containedSecondary: {
          fontWeight: 700,
          backgroundColor: '#ED242E',
          '&:hover': {
              backgroundColor: '#B50015',
          },
        },
      },
      MuiOutlinedInput: {
        root: {
          borderRadius: 0,
          backgroundColor: '#FFFFFF',
        }
      },
      MuiSelect: {
        root: {
          borderRadius: 0,
          backgroundColor: '#FFFFFF',
        }
      },
      MuiCheckbox: {        
        colorSecondary: {   
          '&$checked': {
            color: '#ED242E',
          },        
        },
      },
      MuiToolbar: {
        root: {
          backgroundColor: '#ED242E',
        }
      },
      MuiAlert: {
        root: {
          borderColor: '#ED242E',
        }
      },
      MuiDivider: {
        root: {
          borderColor: '#ED242E',
          borderWidth: 0,
          borderBottomWidth: '0.1rem',
          borderStyle: 'solid',
          backgroundColor: 'transparent',
          marginBottom: "1rem",
        }
      },
    },
    props: {
      MuiButton: {
        disableElevation: true,
      },
    },
    breakpoints: {
      values: {
        xs: 0,
        sm: 768,
        md: 992,
        lg: 1200,
        xl: 1400,
      },
    },
  });